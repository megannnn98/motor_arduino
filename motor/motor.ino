
int a1 = 2;
int b1 = 3;
int a2 = 4;
int b2 = 5;
int btn = 8;
int led = 13;

boolean backward = true;
boolean forward = false;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(a1, OUTPUT);
  pinMode(b1, OUTPUT);
  pinMode(a2, OUTPUT);
  pinMode(b2, OUTPUT);
  pinMode(btn, INPUT_PULLUP);
  
  pinMode(led, OUTPUT);


  forward_backward(forward, 50);
  delay(1000);
  forward_backward(backward, 20);
  delay(1000); 
}

// A2 B2 A1 B1

void setState(boolean sa2, boolean sb2, boolean sa1, boolean sb1)
{
  digitalWrite(a1, sa1);
  digitalWrite(b1, sb1);  
  digitalWrite(a2, sa2);
  digitalWrite(b2, sb2);
 
}


void forward_backward(boolean dir, int lenght)
{

  if (dir != backward)
  {
    for (int i = 0; i < lenght; i++)
    {
      setState(LOW, HIGH, LOW, LOW);     
      delay(2);
      
      setState(LOW, LOW, LOW, HIGH); 
      delay(2);
    
      setState(HIGH, LOW, LOW, LOW);     
      delay(2);
      
      setState(LOW, LOW, HIGH, LOW); 
      delay(2);
    }
  }
  else
  {
    for (int i = 0; i < lenght; i++)
    {
      setState(LOW, LOW, HIGH, LOW); 
      delay(2);
      
      setState(HIGH, LOW, LOW, LOW);     
      delay(2);      
        
      setState(LOW, LOW, LOW, HIGH); 
      delay(2);
      
      setState(LOW, HIGH, LOW, LOW);     
      delay(2);
    }
  }
  setState(LOW, LOW, LOW, LOW);     
}
// 50 cycles is 29 mm
// in 29 mm 50 cycles 
// in 1 mm 50/29 cycles

int ms = 2;
// the loop routine runs over and over again forever:
void loop() {

  if (1 == digitalRead(btn))
  {
    int cnt = 10;
    do {
    forward_backward(forward, 5); 
    forward_backward(backward, 5);
    } while(cnt--);
  
    delay(100);
  }
}














